'use strict';

/**
 * @ngdoc overview
 * @name eidApp
 * @description
 * # eidApp
 *
 * Main module of the application.
 */
 
angular
  .module('eidApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngFileUpload',
    'ui.mask'
  ]);