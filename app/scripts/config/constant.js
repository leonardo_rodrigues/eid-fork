'use strict';

/**
 * @ngdoc Constant
 * @name eidApp.config:Constant
 * @description
 * # Constant
 * Constants of the eidApp
 */
angular.module('eidApp')
  .constant('Config', {
      IS_CORDOVA_APP: document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1,
      EMAIL_VALIDATE_REGEX: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      SENHA_VALIDATE_REGEX: /[\w\d\s-]{6,}/,
      HORA_VALIDATE_REGEX: /([01]\d|2[0-3]):([0-5]\d)/,
      TEL_MASK: '(99) 9999.9999',
      RG_MASK: '99.999.999-9',
      DATA_MASK: '99/99/99',
      HORA_MASK: '99:99',
      CPF_MASK: '999.999.999-99',
      CNPJ_MASK: '99.999.999/9999-99',
      PUBLIC_PATHS: [
      	'/login',
      	'/perfil/new',
      	'/'
      ]
  });