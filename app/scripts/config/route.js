'use strict';

/**
 * @ngdoc route
 * @name eidApp.config:Route
 * @description
 * # Route
 * Router of the eidApp
 */
angular.module('eidApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/perfil/:nuEid', {
        templateUrl: 'views/perfil.html',
        controller: 'PerfilCtrl',
        controllerAs: 'perfil'
      })
      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })
      .when('/pessoas', {
        templateUrl: 'views/pessoas.html',
        controller: 'PessoaCtrl',
        controllerAs: 'pessoa'
      })
      .when('/pessoa/:nuEid', {
        templateUrl: 'views/pessoa.html',
        controller: 'PessoaCtrl',
        controllerAs: 'pessoa'
      })
      .when('/acessos', {
        templateUrl: 'views/acessos.html',
        controller: 'AcessoCtrl',
        controllerAs: 'acesso'
      })
      .when('/acesso/:cdAcesso', {
        templateUrl: 'views/acesso.html',
        controller: 'AcessoCtrl',
        controllerAs: 'acesso'
      })
      .when('/pessoa/acessos/:flTerceiros/:nuEid', {
        templateUrl: 'views/acessoshistorico.html',
        controller: 'AcessoHistoricoCtrl',
        controllerAs: 'acessohistorico'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function(Config, $rootScope, $location) {
      $rootScope.$on('$routeChangeStart', function() {
          if (!$rootScope.principal && Config.PUBLIC_PATHS.indexOf($location.url()) === -1) {
            $location.path('/');
          }       
      });
  });