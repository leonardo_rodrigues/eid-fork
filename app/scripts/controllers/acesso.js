'use strict';

/**
 * @ngdoc function
 * @name eidApp.controller:AcessoCtrl
 * @description
 * # AcessoCtrl
 * Controller of the eidApp
 */
angular.module('eidApp')
  .controller('AcessoCtrl', ['Config',
  							'$scope',
  							'$rootScope',
  							'$routeParams',
  							'AcessoService',
  							'PessoaService',
  							function (Config, $scope, $rootScope, $routeParams, AcessoService, PessoaService) {
  	$scope.isCordovaApp = Config.IS_CORDOVA_APP;
  	$scope.horaMask = Config.HORA_MASK;
  	$scope.dataMask = Config.DATA_MASK;
  	$scope.horaValidateRegex = Config.HORA_VALIDATE_REGEX;
  	$scope.acessos = AcessoService.findAcessosBySituacao($routeParams.flSituacao);
  	$scope.acesso = $routeParams.cdAcesso === 'new' ? {usuario: {eid: {}}} : AcessoService.findAcessoByCdAcesso($routeParams.cdAcesso);
  	$scope.msgSucesso = '';
  	$scope.msgErro = '';

  	$scope.deletarAcesso = function(acesso) {
  		/*AcessoService.delete(acesso);*/
        var indexToDelete = $scope.acessos.indexOf(acesso);
        $scope.acessos.splice(indexToDelete, 1);
  		$scope.msgErro = 'Acesso apagado';
  	};

  	$scope.salvarAcesso = function() {
  		/*AcessoService.save($scope.acesso);*/
  		$scope.acesso.hrEntrada = $scope.acesso.hrEntrada.replace(':', ''); 
  		$scope.msgSucesso = 'Acesso salvo';
  		$scope.acessoForm.$setPristine();
  	};

  	$scope.encontraUsuario = function() {
  		$scope.acesso.usuario = PessoaService.findPessoaByNuEid($scope.acesso.usuario.eid.nuEid);
  	};
	
	$scope.reiniciarUsuario = function() {
		$scope.acesso.usuario = undefined;
	};

	$scope.mostraInfoSemAcessos = function() {
		return $scope.isCordovaApp && $scope.acessos && $scope.acessos.length > 0;
	};

	$scope.acessoAposHorarioAgora = function(acesso) {
		var hoje = new Date();
		if(acesso.dtAcesso.getFullYear() < hoje.getFullYear()) {
			return false;
		}
		if(acesso.dtAcesso.getMonth() < hoje.getMonth()) {
			return false;
		}
		if(acesso.dtAcesso.getDate() < hoje.getDate()) {
			return false;
		}
		var horaValida = acesso.hrEntrada > hoje.getHours()+''+hoje.getMinutes();
		return horaValida;
	};

  	$scope.fechaMsgSucesso = function() {
  		$scope.msgSucesso = '';
  	};

  	$scope.fechaMsgErro = function() {
  		$scope.msgErro = '';
  	};

  	$scope.getNomeBtnSalvar = function() {
  		return $scope.isCordovaApp ? 'Solicite agora' : ($scope.acesso.cdAcesso ? 'Salvar' : 'Adicionar');
  	};

  	/*$scope.$watch('acesso.dtAcesso', function() {
  		if(!$scope.$$phase) {
	  		$scope.$apply(function() {
	  			$scope.acesso.dtAcesso = $scope.formatDate($scope.acesso.dtAcesso);
	  		});
  		}
  	});*/

  }]);
