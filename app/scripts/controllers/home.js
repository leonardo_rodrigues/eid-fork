'use strict';

/**
 * @ngdoc function
 * @name eidApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the eidApp
 */
angular.module('eidApp')
  .controller('HomeCtrl', ['Config',
            							'$scope',
            							'$rootScope',
            							function (Config, $scope, $rootScope) {

  	$scope.pessoa = $rootScope.principal;
  	$scope.isCordovaApp = Config.IS_CORDOVA_APP;

  	$scope.eidAtivo = function() {
  		return $scope.pessoa.eid.flAtivo === 'S' ? 'ativo' : 'inativo';
  	};

    $scope.getEidImagem = function() {
      var deTipoAvatar = $scope.pessoa.deTipoAvatar;
      var deAvatar = $scope.pessoa.deAvatar;
      if(deTipoAvatar && deAvatar) {
        return 'data:' + deTipoAvatar + ';base64,' + deAvatar;
      }
      return '';
    };

    $scope.nuEidSize = function() {
      var size = 70;
      var minNuEidLength = $scope.pessoa.eid.nuEid;
      while(minNuEidLength > 10000) {
        size -= 5;
        minNuEidLength /= 10000;
      }
      return size;
    };
  }]);
