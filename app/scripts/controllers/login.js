'use strict';

/**
 * @ngdoc function
 * @name eidApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the eidApp
 */
angular.module('eidApp')
        .controller('LoginCtrl', ['Config',
            '$q',
            '$location',
            '$scope',
            '$rootScope',
            'LoginService',
            function (Config, $q, $location, $scope, $rootScope, LoginService) {

                $scope.isCordovaApp = Config.IS_CORDOVA_APP;
                $scope.emailValidateRegex = Config.EMAIL_VALIDATE_REGEX;
                $scope.senhaValidateRegex = Config.SENHA_VALIDATE_REGEX;
                $scope.usuario = {};
                $scope.busca = {};

                $scope.msgErro = '';

                $scope.paraCriacaoPerfil = function () {
                    $location.path('/perfil/new');
                };

                $scope.login = function () {
                    LoginService.login($scope.usuario)
                    .then(function(response) {
                        delete $scope.usuario;
                        $rootScope.principal = response.data;
                        $scope.loginForm.$setPristine();
                        $location.path('/home');
                    }, $scope.insereMsgErro);
                };

                $scope.logout = function () {
                    LoginService.logout()
                    .then(function() {
                        $rootScope.principal = undefined;
                        $location.path('/');
                    }, $scope.insereMsgErro);
                };

                $scope.insereMsgErro = function(erro) {
                    $scope.msgErro = erro.data;
                };

                $scope.fechaMsgSucesso = function() {
                    $scope.msgSucesso = '';
                };

                $scope.fechaMsgErro = function() {
                    $scope.msgErro = '';
                };

                $scope.isShowMeusAcessos = function () {
                    return $location.path() === '/acessos' && $scope.isUsuarioLogado() && Config.IS_CORDOVA_APP;
                };

                $scope.isShowAcessos = function () {
                    return $location.path() === '/acessos' && $scope.isUsuarioLogado();
                };

                $scope.isShowEid = function () {
                    return $location.path() === '/pessoas' && $scope.isUsuarioLogado();
                };

                $scope.isShowFooter = function() {
                    return $scope.isShowAcessos() || $scope.isShowEid();
                };

                $scope.isUsuarioLogado = function () {
                    return Boolean($rootScope.principal);
                };

                $scope.isUsuarioLogadoLogin = function () {
                    if(Boolean($rootScope.principal)) {
                        $location.path('/home');
                    }
                };
                
                $scope.getPrincipalCdEid = function() {
                    return $rootScope.principal.cdEid;
                };

                $scope.$watch(function () {
                    return $rootScope.principal;
                }, function () {
                    $scope.usuario = $rootScope.principal;
                }, true);

            }]);
