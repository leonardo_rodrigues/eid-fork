'use strict';

/**
 * @ngdoc function
 * @name eidApp.controller:PerfilCtrl
 * @description
 * # PerfilCtrl
 * Controller of the eidApp
 */
angular.module('eidApp')
  .controller('PerfilCtrl', ['Config',
              							'$q',
              							'$scope',
              							'$rootScope',
              							'$routeParams',
              							'PessoaService',
              							function (Config, $q, $scope, $rootScope, $routeParams, PessoaService) {

  	$scope.telMask = Config.TEL_MASK;
  	$scope.rgMask = Config.RG_MASK;
    $scope.emailValidateRegex = Config.EMAIL_VALIDATE_REGEX;
    $scope.senhaValidateRegex = Config.SENHA_VALIDATE_REGEX;

  	$scope.usuario = $routeParams.nuEid !== 'new' ? PessoaService.findPessoaByNuEid($routeParams.nuEid) : {eid: {flAtivo: 'S', flTipoEid: 'E'}, perfil:{cdPerfil: 1}, flTipo: 'F', nmNFCTag: 'TESTTAG'};
  	$scope.avatar = '';
  	$scope.msgSucesso = '';
  	$scope.msgErro = '';

  	$scope.salvarUsuario = function() {
      $scope.usuario.deBlobUrl = $scope.avatar.$ngfBlobUrl;
      $scope.usuario.deTipoAvatar = $scope.avatar.type;
  		PessoaService.salvar($scope.usuario)
      .then(function(response) {
        $scope.usuario = response.data;
        $scope.msgSucesso = 'Conta criada com sucesso';
      }).catch($scope.insereMsgErro);
  	};

    $scope.insereMsgErro = function (erro) {
        $scope.msgErro = erro.data;
    };

  	$scope.fechaMsgSucesso = function() {
  		$scope.msgSucesso = '';
  	};

  	$scope.fechaMsgErro = function() {
  		$scope.msgErro = '';
  	};

  	$scope.isUsuarioLogado = function() {
  		return $rootScope.principal;
  	};
  }]);