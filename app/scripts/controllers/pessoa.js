'use strict';

/**
 * @ngdoc function
 * @name eidApp.controller:PessoaCtrl
 * @description
 * # PessoaCtrl
 * Controller of the eidApp
 */
angular.module('eidApp')
  .controller('PessoaCtrl', ['Config',
  							'$scope',
  							'$rootScope',
  							'$routeParams',
  							'PessoaService',
  							function (Config, $scope, $rootScope, $routeParams, PessoaService) {
  	$scope.isCordovaApp = Config.IS_CORDOVA_APP;
  	$scope.telMask = Config.TEL_MASK;
  	$scope.pessoas = PessoaService.findAllPessoasCadastradosPorNuEid($rootScope.principal.eid.nuEid);
  	$scope.pessoa = $routeParams.nuEid !== 'new' ? PessoaService.findPessoaByNuEid($routeParams.nuEid) : {};
  	
  	$scope.getIdentMask = function() {
		return $scope.pessoa.flTipo === 'F' ? Config.CPF_MASK : Config.CNPJ_MASK;
  	};

  }]);
