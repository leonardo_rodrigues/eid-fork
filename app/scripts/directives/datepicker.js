'use strict';

/**
 * @ngdoc directive
 * @name eidApp.directive:datepicker
 * @description
 * # datepicker
 */
angular.module('eidApp')
  .directive('datepicker', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, elem, attrs, ngModelCtrl) {
		var updateModel = function (dateText) {
			scope.$apply(function () {
				ngModelCtrl.$setViewValue(dateText);
			});
		};
		var options = {
			dateFormat: 'dd/mm/y',
		    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		    nextText: 'Próximo',
		    prevText: 'Anterior',
			onSelect: function (dateText) {
				updateModel(dateText);
			}
		};
		elem.datepicker(options);
    }
  };
});
