'use strict';

/**
 * @ngdoc directive
 * @name eidApp.directive:eiddata
 * @description
 * # eiddata
 */
angular.module('eidApp')
  .directive('eiddata', function () {
    return {
    	restrict: 'A',
	    require: 'ngModel',
	    link: function (scope, elem, attrs, ngModelCtrl) {
			function daTela(data) {
			    var day = data.substr(0,2);
			    var month = data.substr(2,2);
			    var year = data.substr(4,2);
			    var thisYear = new Date().getFullYear().toString();
			    year = thisYear.substr(0,2)+''+year;
			    return new Date(year, month-1, day);
			}

			function paraTela(data) {
				if(!data) {
					return '';
				}
			    var month = data.getMonth();
			    var day = data.getDate();
			    var year = data.getFullYear();
			    year = year.toString().substr(2,2);

			    month = month + 1;
			    month = month + '';

			    if (month.length === 1) {
			        month = '0' + month;
			    }

			    day = day + '';

			    if (day.length === 1) {
			        day = '0' + day;
			    }
			    return day + '/' + month + '/' + year;
			}
			ngModelCtrl.$parsers.push(daTela);
			ngModelCtrl.$formatters.push(paraTela);
	    }
    };
  });
