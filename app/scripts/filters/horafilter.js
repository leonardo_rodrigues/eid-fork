'use strict';

/**
 * @ngdoc filter
 * @name eidApp.filter:horafilter
 * @function
 * @description
 * # horafilter
 * Filter in the eidApp.
 */
angular.module('eidApp')
  .filter('horafilter', function () {
    return function (hora) {
    	if(hora.length !== 4) {
    		console.log('O atributo hora deve conter 4 caracteres');
    		return hora;
    	}
    	return hora.slice(0, 2)+'h'+hora.slice(2, 4);
    };
  });
