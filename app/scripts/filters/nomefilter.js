'use strict';

/**
 * @ngdoc filter
 * @name eidApp.filter:nomefilter
 * @function
 * @description
 * # nomefilter
 * Filter in the eidApp.
 */
angular.module('eidApp')
  .filter('nomefilter', function () {
    return function (nome, begin, end) {
    	if(end >= nome.length) {
    		return nome;
    	}
      	return nome.slice(begin, end)+'...';
    };
  });
