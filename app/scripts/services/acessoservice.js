'use strict';

/**
 * @ngdoc service
 * @name eidApp.AcessoService
 * @description
 * # AcessoService
 * Service in the eidApp.
 */
angular.module('eidApp')
  .service('AcessoService', ['$http',
  							'PessoaService',
  							function ($http, PessoaService) {
    this.save = function(acesso) {
        acesso = acesso;
    };

    this.findAcessosBySituacao = function(flSituacao) {
    	return this.filtraAcessosBySituacao(flSituacao);
    };

    this.findAcessoByCdAcesso = function(cdAcesso) {
        var acessos = this.getAcessosTest();
        for(var i = 0; i < acessos.length; i++) {
            if(acessos[i].cdAcesso === cdAcesso) {
                return acessos[i];
            }
        }
        return acessos[0];
    };

    this.findAcessosHistoricoByNuEid = function(nuEid) {
        var acessos = this.getAcessosTest();
        for(var i = 0; i < acessos.length; i++) {
            if(acessos[i].pessoa.nuEid === nuEid) {
                return acessos[i];
            }
        }
        return acessos[0];
    };

    this.findAcessosHistoricoAdminByNuEid = function(nuEid) {
        var acessos = this.getAcessosTest();
        for(var i = 0; i < acessos.length; i++) {
            if(acessos[i].pessoa.nuEid === nuEid) {
                return acessos[i];
            }
        }
        return acessos[0];
    };

    this.delete = function(acesso) {
        var acessos = this.getAcessosTest();
        var indexToDelete = acessos.indexOf(acesso);
        acessos.splice(indexToDelete, 1);
    };

    this.filtraAcessosBySituacao = function(flSituacao) {
    	var acessosPorSituacao = [];
    	var acessos = this.getAcessosTest();
    	acessos.forEach(function(acesso) {
    		if(acesso.flSituacao === flSituacao) {
    			acessosPorSituacao.push(acesso);
    		}
    	});
    	return acessosPorSituacao;
    };

    this.getAcessosTest = function() {
    	return [
    	{
    		cdAcesso: 1,
    		hrEntrada: '0930',
    		dtAcesso: new Date(),
    		flSituacao: 'P',
    		usuario: PessoaService.getTestUser()
    	},
    	{
    		cdAcesso: 2,
    		hrEntrada: '1230',
    		dtAcesso: new Date(),
    		flSituacao: 'P',
    		usuario: PessoaService.getTestUser()
    	},
    	{
    		cdAcesso: 3,
    		hrEntrada: '1600',
    		dtAcesso: new Date(),
    		flSituacao: 'P',
    		usuario: PessoaService.getTestUser()
    	},
    	{
    		cdAcesso: 4,
    		hrEntrada: '0100',
    		dtAcesso: new Date(),
    		flSituacao: 'C',
    		usuario: PessoaService.getTestUser()
    	},
    	{
    		cdAcesso: 5,
    		hrEntrada: '0545',
    		dtAcesso: new Date(),
    		flSituacao: 'C',
    		usuario: PessoaService.getTestUser()
    	},
    	{
    		cdAcesso: 6,
    		hrEntrada: '0923',
    		dtAcesso: new Date(),
    		flSituacao: 'C',
    		usuario: PessoaService.getTestUser()
    	},
    	{
    		cdAcesso: 7,
    		hrEntrada: '1003',
    		dtAcesso: new Date(),
    		flSituacao: 'C',
    		usuario: PessoaService.getTestUser()
    	},
    	{
    		cdAcesso: 8,
    		hrEntrada: '0001',
    		dtAcesso: new Date(),
    		flSituacao: 'N',
    		usuario: PessoaService.getTestUser()
    	},
    	{
    		cdAcesso: 9,
    		hrEntrada: '1922',
    		dtAcesso: new Date(),
    		flSituacao: 'N',
    		usuario: PessoaService.getTestUser()
    	}
    	];
    };
  }]);
