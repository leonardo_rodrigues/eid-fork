'use strict';

/**
 * @ngdoc service
 * @name eidApp.LocalFileService
 * @description
 * # LocalFileService
 * Service in the eidApp.
 */
angular.module('eidApp')
  .service('LocalFileService', ['$http',
  								function ($http) {
  	this.getLocalFile = function(blobUrl) {
  		return $http.get(blobUrl, {responseType: 'arraybuffer'})
  		.then(function(response) { 
  			return arrayBufferToBase64(response.data);
  		});
  	};

    var arrayBufferToBase64 = function(buffer) {
      var binary = '';
      var bytes = new Uint8Array(buffer);
      var len = bytes.byteLength;
      for (var i = 0; i < len; i++) {
          binary += String.fromCharCode(bytes[i]);
      }
      return window.btoa(binary);
    };
  }]);
