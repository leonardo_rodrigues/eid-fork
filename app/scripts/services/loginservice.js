'use strict';

/**
 * @ngdoc service
 * @name eidApp.LoginService
 * @description
 * # LoginService
 * Service in the eidApp.
 */
angular.module('eidApp')
  .service('LoginService', ['$http',
  							function ($http) {
    this.login = function(pessoa) {
  		return $http.post('http://localhost:8500/login', pessoa);
  	};

  	this.logout = function() {
  		return $http.get('http://localhost:8500/logout');
  	};
  }]);
