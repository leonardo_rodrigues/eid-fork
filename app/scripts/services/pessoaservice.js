'use strict';

/**
 * @ngdoc service
 * @name eidApp.pessoaservice
 * @description
 * # PessoaService
 * Service in the eidApp.
 */
angular.module('eidApp')
  .service('PessoaService', ['$http',
                            'LocalFileService',
  							             function ($http, LocalFileService) {

  	this.findPessoaByNuEid = function(nuEid) {
  		nuEid = nuEid;
  		return this.getTestUser();
  	};

    this.salvar = function(pessoa) {
      return LocalFileService.getLocalFile(pessoa.deBlobUrl)
      .then(function (data) {
        pessoa.deAvatar = data;
        return $http.post('http://localhost:8500/pessoa/salvar', pessoa);
      });
    };

    this.findAllPessoasCadastradosPorNuEid = function(nuEid) {
      nuEid = nuEid;
      return [
        {
          deSenha: '123',
          flAtivo: 'S',
          deRg: '854563214', 
          deEmail: 'day@email.com',
          nmUsuario: 'Dayane Vareiro da Silva',
          eid: {
            nuEid: 9001,
            flAtivo: 'S',
            cdEid: 1
          },
          passagensCadastradas: [
            {
              cdPassagem: 1,
              nmEidBox: 'Box001',
              flAtivo: 'S',
              endereco: {
                nuCep: '12000-560',
                deLogradouro: 'Rua do Melão'
              },
              eid: {
                flAtivo: 'S',
                nuEid: 5000,
                cdEid: 8
              }
            },
            {
              cdPassagem: 2,
              nmEidBox: 'Box002',
              flAtivo: 'S',
              endereco: {
                nuCep: '56100-810',
                deLogradouro: 'Avenida Arnaldo Jabour'
              },
              eid: {
                flAtivo: 'S',
                nuEid: 5001,
                cdEid: 9
              }
            }
          ],
          cdPessoa: 1
        },
        {
          deSenha: '321',
          flAtivo: 'S',
          deRg: '657891234', 
          deEmail: 'lobo@email.com',
          nmUsuario: 'Lobo Comics',
          eid: {
            flAtivo: 'S',
            nuEid: 9002,
            cdEid: 2
          },
          cdPessoa: 2
        },
        {
          deSenha: '222',
          flAtivo: 'S',
          deRg: '852741963', 
          deEmail: 'someone@email.com',
          nmUsuario: 'Someone There',
          eid: {
            flAtivo: 'S',
            nuEid: 9003,
            cdEid: 3
          },
          passagensCadastradas: [
            {
              cdPassagem: 3,
              nmEidBox: 'Box003',
              flAtivo: 'S',
              endereco: {
                nuCep: '12500-160',
                deLogradouro: 'Rua do Esporto'
              },
              eid: {
                flAtivo: 'S',
                nuEid: 5003,
                cdEid: 10
              }
            }
          ],
          cdPessoa: 3
        }
      ];
    };

  	this.getTestUser = function() {
  		return {
  			deSenha: 'senha123',
  			flAtivo: 'S',
  			deRg: '451234567', 
  			deEmail: 'leonardo@email.com',
  			nmUsuario: 'Leonardo Rodrigues da Silva',
  			eid: {
          flAtivo: 'S',
          nuEid: 9888,
          cdEid: 4
        },
        cdPessoa: 4
  		};
  	};

  	this.getOtherTestUser = function() {
  		return {
  			deSenha: '123456',
  			flAtivo: 'S',
  			deRg: '458652026', 
  			deEmail: 'dayane@email.com',
  			nmUsuario: 'Dayane Vareiro',
  			eid: {
          flAtivo: 'S',
          nuEid: 5888,
          cdEid: 5
        },
        cdPessoa: 5
  		};
  	};
  }]);
