'use strict';

var CordovaInit = function () {
	var isCordovaApp = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;

	if (isCordovaApp) {
		document.addEventListener('deviceready', function() {
			console.log('Cordova detectado, inicializando Angular manualmente.');
			angular.bootstrap(document, ['eidApp']);
		}, false);
	} else {
		console.log('Cordova não detectado, inicializando Angular manualmente.');
		angular.bootstrap(document, ['eidApp']);
	}
};

angular.element(document).ready(function() {
	new CordovaInit();
});