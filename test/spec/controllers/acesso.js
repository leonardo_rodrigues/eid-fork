'use strict';

describe('Controller: AcessoCtrl', function () {

  // load the controller's module
  beforeEach(module('eidApp'));

  var AcessoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AcessoCtrl = $controller('AcessoCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AcessoCtrl.awesomeThings.length).toBe(3);
  });
});
