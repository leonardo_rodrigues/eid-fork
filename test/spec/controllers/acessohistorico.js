'use strict';

describe('Controller: AcessohistoricoCtrl', function () {

  // load the controller's module
  beforeEach(module('eidApp'));

  var AcessohistoricoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AcessohistoricoCtrl = $controller('AcessohistoricoCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AcessohistoricoCtrl.awesomeThings.length).toBe(3);
  });
});
