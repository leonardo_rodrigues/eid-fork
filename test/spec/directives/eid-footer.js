'use strict';

describe('Directive: eidFooter', function () {

  // load the directive's module
  beforeEach(module('eidApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<eid-footer></eid-footer>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the eidFooter directive');
  }));
});
