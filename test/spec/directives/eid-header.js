'use strict';

describe('Directive: eidHeader', function () {

  // load the directive's module
  beforeEach(module('eidApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<eid-header></eid-header>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the eidHeader directive');
  }));
});
