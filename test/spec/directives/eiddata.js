'use strict';

describe('Directive: eiddata', function () {

  // load the directive's module
  beforeEach(module('eidApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<eiddata></eiddata>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the eiddata directive');
  }));
});
