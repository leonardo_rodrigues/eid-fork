'use strict';

describe('Filter: horafilter', function () {

  // load the filter's module
  beforeEach(module('eidApp'));

  // initialize a new instance of the filter before each test
  var horafilter;
  beforeEach(inject(function ($filter) {
    horafilter = $filter('horafilter');
  }));

  it('should return the input prefixed with "horafilter filter:"', function () {
    var text = 'angularjs';
    expect(horafilter(text)).toBe('horafilter filter: ' + text);
  });

});
