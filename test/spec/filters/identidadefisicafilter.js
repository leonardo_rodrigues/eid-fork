'use strict';

describe('Filter: identidadefisicafilter', function () {

  // load the filter's module
  beforeEach(module('eidApp'));

  // initialize a new instance of the filter before each test
  var identidadefisicafilter;
  beforeEach(inject(function ($filter) {
    identidadefisicafilter = $filter('identidadefisicafilter');
  }));

  it('should return the input prefixed with "identidadefisicafilter filter:"', function () {
    var text = 'angularjs';
    expect(identidadefisicafilter(text)).toBe('identidadefisicafilter filter: ' + text);
  });

});
