'use strict';

describe('Filter: identidadejuridicafilter', function () {

  // load the filter's module
  beforeEach(module('eidApp'));

  // initialize a new instance of the filter before each test
  var identidadejuridicafilter;
  beforeEach(inject(function ($filter) {
    identidadejuridicafilter = $filter('identidadejuridicafilter');
  }));

  it('should return the input prefixed with "identidadejuridicafilter filter:"', function () {
    var text = 'angularjs';
    expect(identidadejuridicafilter(text)).toBe('identidadejuridicafilter filter: ' + text);
  });

});
