'use strict';

describe('Filter: nomefilter', function () {

  // load the filter's module
  beforeEach(module('eidApp'));

  // initialize a new instance of the filter before each test
  var nomefilter;
  beforeEach(inject(function ($filter) {
    nomefilter = $filter('nomefilter');
  }));

  it('should return the input prefixed with "nomefilter filter:"', function () {
    var text = 'angularjs';
    expect(nomefilter(text)).toBe('nomefilter filter: ' + text);
  });

});
