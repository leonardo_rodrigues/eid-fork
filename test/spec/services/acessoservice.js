'use strict';

describe('Service: acessoservice', function () {

  // load the service's module
  beforeEach(module('eidApp'));

  // instantiate service
  var acessoservice;
  beforeEach(inject(function (_acessoservice_) {
    acessoservice = _acessoservice_;
  }));

  it('should do something', function () {
    expect(!!acessoservice).toBe(true);
  });

});
