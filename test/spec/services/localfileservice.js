'use strict';

describe('Service: LocalFileService', function () {

  // load the service's module
  beforeEach(module('eidApp'));

  // instantiate service
  var LocalFileService;
  beforeEach(inject(function (_LocalFileService_) {
    LocalFileService = _LocalFileService_;
  }));

  it('should do something', function () {
    expect(!!LocalFileService).toBe(true);
  });

});
