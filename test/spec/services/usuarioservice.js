'use strict';

describe('Service: usuarioservice', function () {

  // load the service's module
  beforeEach(module('eidApp'));

  // instantiate service
  var usuarioservice;
  beforeEach(inject(function (_usuarioservice_) {
    usuarioservice = _usuarioservice_;
  }));

  it('should do something', function () {
    expect(!!usuarioservice).toBe(true);
  });

});
